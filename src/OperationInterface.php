<?php
namespace Drupal\operations;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for Operations.
 */
interface OperationInterface extends PluginInspectionInterface {

  /**
   * Show a confirm form for the operation.
   *
   * The confirm form will be shown just before the operation is fired to allow
   * options on executing the operation.
   *
   * @param array $form
   *   The form array.
   * @param FormStateInterface $form_state
   *   The form state object.
   * @param EntityInterface $entity
   *   The entity the operation is being performed on.
   *
   * @return array
   *   The confirm form array.
   */
  public function form(array $form, FormStateInterface $form_state, EntityInterface $entity);

  /**
   * Submit the confirm form for the operation.
   *
   * This should set any operation options into the 'operation_options' key on
   * the form_state object.
   *
   * @param array $form
   *   The form array.
   * @param FormStateInterface $form_state
   *   The form state object.
   */
  public function formSubmit(array $form, FormStateInterface $form_state);

  /**
   * Get default options before doing the operation.
   *
   * @param EntityInterface $entity
   *   The entity to perform the operation on.
   *
   * @return array
   *   An updated array of options.
   */
  public function defaultOptions(EntityInterface $entity);

  /**
   * Prepare options before doing the operation.
   *
   * @param EntityInterface $entity
   *   The entity to perform the operation on.
   * @param array $options
   *   Provided options, defaults to array.
   *
   * @return array
   *   An updated array of options.
   */
  public function prepareOptions(EntityInterface $entity, array $options = []);

  /**
   * Do the operation.
   *
   * @param EntityInterface $entity
   *   The entity to perform the operation on.
   * @param array $options
   *   One time options for this operations. These can be set using form.
   */
  public function do(EntityInterface $entity, array $options = []);

  /**
   * Validate the operation.
   *
   * The reasons array is passed by reference. If reasons is still empty at the
   * end of this function then the operation is considered valid and will be
   * run.
   *
   * @param EntityInterface $entity
   *   The entity to perform the operation on.
   * @param array $options
   *   One time options for this operations. These can be set using form.
   * @param array &$reasons
   *   Reasons why the operation cannot be performed.
   */
  public function validate(EntityInterface $entity, array $options = [], array &$reasons = []);

  /**
   * Constrain the operation.
   *
   * Constraints differ from validation in that a constrained operation will
   * never be shown as an option to the user. Validation on the other hand will
   * be performed when the user attempts to run the operation.
   *
   * @param EntityInterface $entity
   *   The entity we want to perform the operation on.
   *
   * @return boolean
   *   TRUE if the operation is available, FALSE otherwise.
   */
  public function available(EntityInterface $entity);
}
