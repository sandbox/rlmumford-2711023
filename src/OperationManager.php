<?php
namespace Drupal\operations;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityInterface
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manager for operation plugins.
 */
class OperationManager extends DefaultPluginManager {

  /**
   * Cached array of operation instances.
   *
   * @var \Drupal\operations\OperationInterface[]
   */
  protected $instances;

  /**
   * Cached array of category labels keyed by machin name.
   *
   * @var array
   */
  protected $categories;

  /**
   * Constructs a new \Drupal\operations\OperationManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Operation', $namespaces, $module_handler, 'Drupal\operations\OperationInterface', 'Drupal\operations\Annotation\Operation');

    $this->alterInfo('operations');
    $this->setCacheBackend($cache_backend, 'operation_plugin');
  }

  /**
   * Get categories.
   */
  public function getCategories() {
    if (!empty($this->categories)) {
      return $this->categories;
    }

    if ($cache = $this->cacheBackend->get($this->cacheKey . '::categories')) {
      $this->categories = $cache->data;
    }
    else {
      $this->categories = $this->findCategories();
      $this->cacheBackend->set($this->cacheKey . '::categories', $this->categories, Cache::PERMANENT, array('operations'));
    }
    return $this->categories;
  }

  /**
   * Find category definitions.
   */
  protected function findCategories() {
    $categories = $this->moduleHandler->invokeAll('operation_category_info');
    $this->moduleHandler->alter('operation_category_info', $categories);
    return $categories;
  }

  /**
   * Get all available operation plugins for a given Entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity the operation will act on.
   * @param string $category (optional)
   *   The category of operations to get.
   *
   * @return \Drupal\operations\OperationInterface[]
   */
  public function getEntityOperations(EntityInterface $entity, $category = NULL) {
    $operations = [];
    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if (!empty($category) && $definition['category'] != $category) {
        continue;
      }

      if (!isset($this->instances[$plugin_id])) {
        $this->instances[$plugin_id] = $this->createInstance($plugin_id);
      }

      $operation = $this->instances[$plugin_id];
      if ($operation->available($entity)) {
        $operations[$plugin_id] = $operation;
      }
    }
    return $operations;
  }
}