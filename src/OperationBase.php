<?php
namespace Drupal\operations;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base Class for Operations.
 */
interface OperationBase extends PluginBase implements OperationInterface {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state, EntityInterface $entity) {
    $form['message'] = [
      '#markup' => $this->t('Are you sure you want to perform this operation?'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function formSubmit(array $form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function defaultOptions(EntityInterface $entity) {
    $default_options = [];
    if (!empty($this->configuration['options'])) {
      $default_options += $this->configuration['options'];
    }
    if (!empty($this->pluginDefinition['options'])) {
      $default_options += $this->pluginDefinition['options'];
    }
    return $default_options;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareOptions(EntityInterface $entity, array $options = []) {
    // Merge the passed in options with the default options.
    return $options + $this->defaultOptions($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function do(EntityInterface $entity, array $options = []) {
    $options = $this->prepareOptions($entity, $options);
    $this->preOperation($entity, $options);
    $this->doOperation($entity, $options);
    $this->postOperation($entity, $options);
  }

  /**
   * Act before an operation.
   *
   * @param EntityInterface $entity
   *   The entity to perform the operation on.
   * @param array $options
   *   One time options for this operations. These can be set using form.
   */
  protected function preOperation(EntityInterface $entity, array $options = []) {
    // @todo: Add hook.
  }

  /**
   * Act just after an operation.
   *
   * @param EntityInterface $entity
   *   The entity to perform the operation on.
   * @param array $options
   *   One time options for this operations. These can be set using form.
   */
  protected function postOperation(EntityInterface $entity, array $options = []) {
    // @todo: Add hook.
  }

  /**
   * Actually perform the operation.
   *
   * @param EntityInterface $entity
   *   The entity to perform the operation on.
   * @param array $options
   *   One time options for this operations. These can be set using form.
   */
  protected abstract function doOperation(EntityInterface $entity, array $options = []);

  /**
   * Validate the operation.
   *
   * The reasons array is passed by reference. If reasons is still empty at the
   * end of this function then the operation is considered valid and will be
   * run.
   *
   * @param EntityInterface $entity
   *   The entity to perform the operation on.
   * @param array $options
   *   One time options for this operations. These can be set using form.
   * @param array &$reasons
   *   Reasons why the operation cannot be performed.
   */
  public function validate(EntityInterface $entity, array $options = [], array &$reasons = []) {}

  /**
   * Constrain the operation.
   *
   * Constraints differ from validation in that a constrained operation will
   * never be shown as an option to the user. Validation on the other hand will
   * be performed when the user attempts to run the operation.
   *
   * @param EntityInterface $entity
   *   The entity we want to perform the operation on.
   *
   * @return boolean
   *   TRUE if the operation is available, FALSE otherwise.
   */
  public function available(EntityInterface $entity) {}
}
