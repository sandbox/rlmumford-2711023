<?php
namespace Drupal\operations\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a operation annotation object.
 *
 * Plugin Namespace: Plugin\Operation
 *
 * @see \Drupal\operations\OperationBase
 * @see \Drupal\operations\OperationManager
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class Operation extends Plugin {

  /**
   * The operation plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the operation.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * The human-readable verb or verb phrase describing the operation.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $verb;

  /**
   * The category of the operation.
   *
   * @var string
   */
  public $category;

  /**
   * An array of options for defaults.
   *
   * @var array
   */
  public $options = [];

  /**
   * An array of constraints for the operation.
   *
   * @var array
   */
  public $contraints = [];

  /**
   * An array of validators for the operation.
   *
   * @var array
   */
  public $validators = [];
}
